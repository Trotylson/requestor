import random

class Logo():
    def __init__(self):
        self.logos = []

        self.logos.append(f"""
                            ----------------------
                                 ~REQUESTOR~
                                    v.01
                            ----------------------
""" )

        self.logos.append(f"""
                            _____________________
                            |                   |
                            |  ***REQUESTOR***  |
                            |       v.01        |
                            |___________________|
""")

    def print_logo(self):
        print(random.choice(self.logos))
