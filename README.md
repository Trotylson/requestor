# `REQUESTOR`

author: `trotyl`

version: `0.1`

powered by: `python3`
#


## `'readme.md' content`
1. instalation
2. usage emaples

#
#

# `1. Instalation`

## **`Before repository cloning`**


Create a new directory for script and move to this directory, eg.:

    mkdir $HOME/requestor/ && cd $HOME/requestor/

Next clone the script from git repository being in new directory:

    git clone https://gitlab.com/Trotylson/requestor.git

Then:

    cd requestor/
#

## **`After repository cloning`**

Take in mind that this script can be run by TOR connection and the proxy servers with proxychains4. You have 2 ways to do this.
#

***`FIRST METHOD` - using bash script*** (this option do everythink automaticly):

Being still in new directory run bash script named comply.sh:

    ./comply.sh
        or
    bash comply.sh

#

### **`SECOND METHOD` - make it manually:**

### ***`DEBIAN DISTRIBUTIONS`***

**without proxy coverage:**

It's only about adding alias to ~/.bash_aliases

    sudo echo "alias requestor='python3 $PWD/main.py'" >> ~/.bash_aliases && . ~/.bash_aliases


**with proxy coverage:**

You first need to install tor and proxychains4 packets and configure proxychains4.conf:

    sudo apt-get install proxychains4

(proxychains4.conf configuration <a href="https:///www.proxychains4.com">here</a>)

Then **`TOR`** service:

    sudo apt-get install tor

After instalation and configuration start tor service:

    sudo systemctl start tor
    sudo systemctl enable tor

...and then check is it works:

    sudo systemctl status tor

If service work you schould se something similar to this:

    tor.service - Anonymizing overlay network for TCP (multi-instance-master)
     Loaded: loaded (/lib/systemd/system/tor.service; enabled; vendor preset: e>
     Active: active (exited) since Wed 2023-03-08 10:09:55 CET; 18min ago
    Process: 876 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
    Main PID: 876 (code=exited, status=0/SUCCESS)
        CPU: 4ms

So if status is **`active (exited)`**, means service is active and working and you can jump to the next step.

Create aliases:

    sudo echo "alias requestor='python3 $PWD/main.py'" >> ~/.bash_aliases && echo "alias proxy4requestor='proxychains4 python3 $PWD/main.py'" >> ~/.bash_aliases && . ~/.bash_aliases

#

### ***`ARCH DISTRIBUTIONS`***

**without proxy coverage:**

It's only about adding alias to ~/.bash_aliases

    sudo echo "alias requestor='python3 $PWD/main.py'" >> ~/.bash_aliases && . ~/.bash_aliases


**with proxy coverage:**

You first need to install tor and proxychains4 packets and configure proxychains4.conf.

**`TOR`** service:

    sudo pacman -Syu tor

After instalation and configuration start tor service:

    sudo systemctl start tor
    sudo systemctl enable tor

...and then check is it works:

    sudo systemctl status tor

If service work you schould se something similar to this:

    tor.service - Anonymizing overlay network for TCP (multi-instance-master)
     Loaded: loaded (/lib/systemd/system/tor.service; enabled; vendor preset: e>
     Active: active (running) since Wed 2023-03-08 10:09:55 CET; 18min ago
    Process: 876 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
    Main PID: 876 (code=exited, status=0/SUCCESS)
        CPU: 4ms

So if status is **`active (running)`**, means service is active and working.

Now proxychains4:

Clone git repository and follow readme.md instruction:

visit:
    https://archlinux.org/packages/community/x86_64/proxychains-ng/

or

direct clone: 
    
    git clone https://github.com/rofl0r/proxychains-ng.git

Then configure proxychains4.conf file (example: <a href="https:///www.proxychains4.com">here</a>)


Create aliases:

    sudo echo "alias requestor='python3 $PWD/main.py'" >> ~/.bash_aliases && echo "alias proxy4requestor='proxychains4 python3 $PWD/main.py'" >> ~/.bash_aliases && . ~/.bash_aliases

#

Now just type in console command `requestor -U http://google.com`

    user@matrix:~$ requestor -U http://www.google.com

    
                            _____________________
                            |                   |
                            |  ***REQUESTOR***  |
                            |       v.01        |
                            |___________________|


    PROTOCOL:       HTTP
    STATUS CODE:    200
    METHOD:         GET

    =======================================================================



or if you want to request by proxies use `proxy4requestort -U http://google.com`:

    user@matrix:~$ proxy4requestor -U http://google.com

    [proxychains] config file found: /etc/proxychains4.conf
    [proxychains] preloading /usr/lib/x86_64-linux-gnu/libproxychains.so.4
    [proxychains] DLL init: proxychains-ng 4.14

                                ----------------------
                                    ~REQUESTOR~
                                        v.01
                                ----------------------

    [proxychains] Dynamic chain  ...  127.0.0.1:9050  ...  127.0.0.1:9050 <--denied
    [proxychains] Dynamic chain  ...  127.0.0.1:9050  ...  google.com:80  ...  OK
    [proxychains] Dynamic chain  ...  127.0.0.1:9050  ...  www.google.com:80  ...  OK

    PROTOCOL:       HTTP
    STATUS CODE:    200
    METHOD:         GET

    =======================================================================


Follow `-h` switch:

    user@matrix:~$ requestor -h

## `2. Usage examples`

- show script options:

        requestor -h

- request to check availability (always GET method if you don't specify another):

        requestor -U https://google.com

- request with POST method and headers:

        requestor -U https://google.com -M post -H "{'authorization': 'Bearer token'}"

- request with PUT method, headers and body/data parameters:

        requestor -U https://google.com -M put -H "{'authorization': 'Bearer token'}" -D "{'name': 'anon', 'value': 'ultimate'}"

- request with DELETE method with body/data parameters:

        requestor -U https://google.com -M delete -H "{'authorization': 'Bearer token'}" -D "{'id': 1}"

`Also for all requests you can set that do you want see response headers or content by '-rh' or/and '-rc' switches`:

    requestor -U https://google.com -rh -rc



#
#

## `changelog`

|ver.|   date   |type|description|
|:--:|:--------:|:--:|-----------|
| 0.1|2023.03.08|init|initial version| 