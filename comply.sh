#!/bin/bash

DISTRO=$(lsb_release -i -s)

if [[ $DISTRO == 'Debian' ]]; then
	echo "Debian detected."
	sudo apt-get install python3-pip
	pip install requests
	echo "[ * ]	Trying to install proxychains4..."
	sudo apt-get install proxychains4

	echo "[ * ]	Trying to install tor..."
	sudo apt-get install tor

	sudo apt-get update

elif [[ $DISTRO == 'Arch' ]]; then
 	echo "Arch detected."
	sudo pacman -Sy python-pip
	pip install requests
	sudo pacman -Sy tor
else
	"[ ! ]	COMPLY.SH SCRIPT NOT SUPPORT THIS OS - please comply manualy following readme.md"
fi


echo "alias requestor='python3 $PWD/main.py'" >> ~/.bash_aliases
echo "alias proxy4requestor='proxychains4 python3 $PWD/main.py'" >> ~/.bash_aliases
. ~/.bash_aliases

echo "[ OK ]	Done"

