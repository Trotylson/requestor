import argparse
import os
import json


parser = argparse.ArgumentParser(
                    prog = 'requestor',
                    description = 'Simple http / https request tool with get, post, put and delete methods for endpoint tests.',
                    epilog = 'Created by trotyl || Powered by Python3')

parser.add_argument(
    "-U", "--url",
    required=False,
    type=str,
    help="set url to send request")

parser.add_argument(
    "-M", "--method",
    required=False,
    type=str,
    default="GET",
    help="set request method [GET, POST, PUT, DELETE] selection. Default is GET")

parser.add_argument(
    "-H", "--headers",
    required=False,
    type=json.loads,
    help="set request headers, eg. {'authorization': 'bearer token'}")

parser.add_argument(
    "-D", "--data",
    required=False,
    type=json.loads,
    help="set request data, eg. {'foo': 'bar'}")


parser.add_argument(
    "-rh", "--responseheader",
    required=False,
    action='store_true',
    help="shows response headers")

parser.add_argument(
    "-rc", "--responsecontent",
    required=False,
    action='store_true',
    help="shows response content")

parser.add_argument(
    "--memo",
    required=False,
    help="""
            specify the id of used request, eg. '--memo 7' to run request with id 7.
            You can check saved ID's and requests by using '--memo list'.
            Using '--memo save' will save request configuration.
            Using '--memo last' will run last saved request.
    """)


parser.add_argument(
    "-oH", "--outputheader",
    required=False,
    type=str,
    help="path to save response headers")

parser.add_argument(
    "-oC", "--outputcontent",
    required=False,
    type=str,
    help="path to save response content")

parser.add_argument(
    "-oA", "--outputall",
    required=False,
    type=str,
    help="path to save response headers and content")

execparser = parser.parse_args()
