
import json
import requests
import re


class Requestor():
    def __init__(self, args) -> None:
        self.session = requests.Session()
        self.arg = args
        self.proto_dict = {}
        self.user_proto = False

        self.proto = ''
        self.status_code = ''
        self.headers = ''
        self.content = ''

        self.response_content = []

    def create_response_dict(self):
        try:
            content = self.content.decode()
        except UnicodeDecodeError:
            content = self.content

        response_dict = {
            'protocol': self.proto,
            'status_code': self.status_code,
            'headers': self.headers,
            'content': content
        }
        return response_dict

    def make_request(self, url, method='GET', data=None, headers=None):
        try:
            response = requests.request(method, url, data=data, headers=headers)
            if response.ok:
                return response
            else:
                print(f"status code: {response.status_code}")
        except requests.exceptions.RequestException as e:
            print(f"Exception occure:\n{e}")

    def check_url(self, url):
        """
        Checking is the url is valid. 
        (have http:// or https:// prefix in address)
        """
        if 'http' in url:
            print('URL:', url)
            self.user_proto = True
            try:
                proto = re.findall(r'^(http.*?)(://.+)', url)
                self.proto_dict[f'{proto[0][0]}'] = url
                return self.proto_dict
            except IndexError:
                print(f"\n\n[!] Invalid url or its configuration!\n\n")
                exit()
        
        self.proto_dict['https']='https://' + url
        self.proto_dict['http']='http://' + url
        return self.proto_dict
    
    def check_response(self, protocol, response):
        self.proto = protocol
        try:
            self.status_code = response.status_code
            self.headers = response.headers
            self.content = response.content
        except Exception as e:
            print(f"Exception occure:\n{e}")
            exit(1)

    def check_args(self):
        report = {
            'report': [],
            'errors': []
        }

        # try:
        #     self.arg.headers = json.loads(self.arg.headers)
        # except Exception as e:
        #     report['errors'].append(f"[!] header error: {e}")

        # try:
        #     self.arg.data = str(self.arg.data)
        # except Exception as e:
        #     report['errors'].append(f"[!] body/data error: {e}")

        for key, value in vars(self.arg).items():
            # print(key, value)
            try:
                if value:
                    report['report'].append(f"[OK] {key} switch detected")
                else:
                    report['report'].append(f"[*]no {key} switch detected")
            except Exception as e:
                report['errors'].append(f"[!] {key} error")
        print()
        return report

    def use_fuse(self, url):
        flag = False
        url = self.check_url(url)
        report = self.check_args()

        print("switches I/O:")
        for x in report['report']:
            print("\t", x)
        for x in report['errors']:
            # print(x)
            flag = True
        print()
        return [flag, report['errors'], url]
    
    def shoot(self, url):
        response_list = []
        # print(json.dumps(self.arg.headers))
        # exit()
        # url = self.check_url(url)
        fuse = self.use_fuse(url)
        url = fuse[2]
        if fuse[0]:
            print(f"\n\n\t\t\t[->|<-] FUSE BREAK DETECTED!\n")
            for x in fuse[1]:
                print("  -", x)
            exit()
        for protocol in url.keys():
            response = self.make_request(
                url=url[protocol], 
                method=self.arg.method, 
                data=self.arg.data, 
                headers=self.arg.headers
            )
            if response:
                self.check_response(protocol, response)
                response_list.append(self.create_response_dict())
        return response_list


class Adjuster(Requestor):
    def __init__(self, args):
        self.arg = args

    def cli_show(self, response):
        print(f"\nPROTOCOL:       {response['protocol'].upper()}")
        print(f"STATUS CODE:    {response['status_code']}")
        print(f"METHOD:         {self.arg.method.upper()}")
        if self.arg.responseheader:
            print(f"\nHEADER:         \n{json.dumps(dict(response['headers']), indent=4)}")
        if self.arg.responsecontent:
            print(f"\n\nCONTENT:    \n\n{response['content']}")
        print("\n=======================================================================\n")