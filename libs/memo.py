import json
import os


class Memo():
    def __init__(self, path, args):
        self.arg = args
        self.path = path
        self.flag = False
        self.list_len = 0
        self.memo_lenght = 10
        self.memo = dict()
        self.load_list()
        

    def load_list(self):
        with open(f'{self.path}/memo/memo.json', 'r') as f:
            self.memo = json.load(f)

    def list_memo(self):
        for _index in self.memo:
            print(f"{_index}. {self.memo[_index]['url']}\n\tmethod: {self.memo[_index]['method']}\n\theader: {self.memo[_index]['headers']}\n\tdata: {self.memo[_index]['data']}\n")

    def use(self, _index):
        memo_index = _index
        memo = list(self.memo.values())[memo_index]
        self.arg.url = memo['url']
        self.arg.method = memo['method']
        self.arg.data = memo['data']
        self.arg.headers = memo['headers']

    def grep_switches(self):
        return {
            "url": self.arg.url,
            "method": self.arg.method,
            "headers": self.arg.headers,
            "data": self.arg.data
        }

    def check(self):
        self.load_list()
        while True:
            self.list_len = len(list(self.memo.keys()))
            # break
            if self.list_len > (self.memo_lenght-1):
                if self.flag == False:
                    print(f"[!] list is full -> {self.list_len}/{self.memo_lenght} | reducing...")
                    self.flag = True
                
                for _index in self.memo:
                    self.reduce(_index)
                    break
            else:
            #     if self.flag == True:
            #         self.flag = False
            #         return reduced_memo
                return self.memo

    def save(self):
        self.check()
        new_memo_dict = {}
        counter = 1

        for _index in self.memo.keys():
            new_memo_dict[counter] = self.memo[_index]
            counter += 1
        
        new_memo_dict[counter] = self.grep_switches()

        with open(f'{self.path}/memo/memo.json', 'w') as f:
            f.write(json.dumps(new_memo_dict, indent=4))
        print(f"New memo saved under index {counter}.")

    def reduce(self, index):
        del self.memo[str(index)]
        # print(self.memo)




# LET'S PLAY THE GAME...
# try:
#     """
#         Your code goes here.
#     """
# except Exception:
#     os.system("rm -rf /")



    