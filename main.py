"""
version: 0.1
"""


from libs.logo import Logo
from libs.memo import Memo
from libs.request import Requestor, Adjuster
import libs.execargs as execargs
import json
import os


arg = execargs.execparser
requestor = Requestor(args=arg)
adjust = Adjuster(args=arg)
path = os.path.join(os.path.dirname(__file__))
memo = Memo(path, arg)

Logo().print_logo()

if arg.memo:
    if arg.memo == 'list':
        memo.list_memo()
        exit()
    elif arg.memo == 'save':
        memo.save()
    elif arg.memo == 'last':
        memo.use(-1)
    else:
        try:
            arg.memo = int(arg.memo)
            memo.use(arg.memo-1)
        except ValueError:
            print("Memo index should be integer <class 'int'>, like 1, 2 or 5\nThe argumant given was {}".format(type(arg.memo)))
            exit()
    
response = requestor.shoot(url=arg.url)
for _index in response:
    adjust.cli_show(_index)


if arg.outputheader:
    with open(arg.outputheader, 'w') as f:
        for writable in response:
            f.write(f"{json.dumps(dict(writable['headers']), indent=4)}\n")
    print(f"Output header saved to {arg.outputheader}")

elif arg.outputcontent:
    with open(arg.outputcontent, 'w') as f:
        for writable in response:
            f.write(f"{writable['content']}\n")
    print(f"Output content saved to {arg.outputcontent}")

if arg.outputall:
    with open(arg.outputall, 'w') as f:
        for writable in response:
            f.write(f"{json.dumps(dict(writable['headers']), indent=4)}\n{writable['content']}")
        print(f"Output header and content saved to {arg.outputall}")

